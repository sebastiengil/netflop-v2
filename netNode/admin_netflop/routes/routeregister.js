var express = require('express');
var register = express.Router();

// Require controller modules
var controller_register = require('../controllers/controller_register');

//GET request for list of all items


register.get('/', controller_register.register);

register.post('/new_user', controller_register.newUser)

module.exports = register;