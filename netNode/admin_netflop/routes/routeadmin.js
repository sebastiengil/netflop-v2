var express = require('express');
var admin = express.Router();

var controller_admin = require('../controllers/controller_admin');



admin.get('/', controller_admin.index);
admin.post('/', controller_admin.login);
admin.get('/logout', controller_admin.logout);
admin.get('/admin', controller_admin.indexadmin);

admin.get('/films', controller_admin.filmlist);
admin.get('/film?:id', controller_admin.filmFromDb);
admin.post('/films/add', controller_admin.addFilm);
admin.post('/films/delete', controller_admin.deleteFilm);
admin.get('/films/edit', controller_admin.editFilm);

admin.get('/user', controller_admin.userlist);
admin.post('/delete', controller_admin.deleteUser);
admin.get('/edit', controller_admin.editUser);

module.exports = admin;