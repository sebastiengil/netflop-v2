var request = require('request');

var sess;



exports.index = (req, res, next) => {
    sess = req.session;
    //! logué
    if (sess.login) {
        return res.redirect('/admin');
    }
    //! pas logué
    res.render('login');
};


exports.login = function (req, res, next) {
    sess = req.session
    let form = req.body;
    //! Connection a la base de donnée
    request.post({
        url: 'http://localhost:4000/login',
        form: form
    }, (error, response, body) => {
        if (body === "") {
            return res.render('login', {
                message: "BAD Login ou password"
            })
        }
        sess.login = req.body.login;
        res.redirect('/')
    })

};

exports.logout = function (req, res, next) {

    req.session.destroy((err) => {
        if (err) {
            return console.log(err);
        }
        res.redirect('/');
    });
};


// localhost:3000/admin
exports.indexadmin = (req, res, next) => {
    sess = req.session;
    res.render('index');
};

//! Creation du tableau des utilisateur !
// localhost:3000/user
exports.userlist = function (req, res, next) {
    sess = req.session;
    request.get("http://localhost:4000/users", function (req1, res1, body) {
        res.render('user', {
            listUsers: JSON.parse(res1.body)
        })

    })
};


//! supression d'un utilisateur
exports.deleteUser = function (req, res, next) {
    sess = req.session
    let userId = req.body.id;
    console.log(userId);
    request.del({
        url: 'http://localhost:4000/users/' + userId
    }, (error, response, body) => {
        res.redirect('/user');
    })
};

exports.editUser = function (req, res, next) {
    sess = req.session
    let form = req.query

    request.put({
        url: 'http://localhost:4000/users/',
        form: form
    }, (error, response, body) => {
        request.get("http://localhost:4000/users", function (req2, res2, body2) {
            res.render('user', {
                listUsers: JSON.parse(res2.body),
                message: "Utilisateur édité"
            })
        })
    })
};



//! Creation du tableau des film !
// localhost:3000/film
exports.filmlist = function (req, res, next) {
    sess = req.session;

    //res.render('film')
    request.get("http://localhost:4000/movies", function (req1, res1, body) {

        res.render('film', {
            listFilm: JSON.parse(res1.body)
        })
            
    })
}


// localhost:3000/film requete OMDB
exports.filmFromDb = function (req, res, next) {
    sess = req.session;
    var recherche = req.query.recherche
    request.get({
        url: 'http://www.omdbapi.com/?apikey=8675fc12&s=' + recherche
    }, function (err, response, body) {
        let films = JSON.parse(body)
        request.get("http://localhost:4000/movies", function (req1, res1, body) {
            res.render('film', {
                films: films.Search,
                listFilm: JSON.parse(res1.body)
            });
        })
    })
}


//! Ajout d'un film
exports.addFilm = function (req, res, next) {
    sess = req.session;
    var add = req.body
    request.post({
        url: "http://localhost:4000/movies",
        form: add
    }, function (req1, res1, body) {
        res.redirect('/films')
    })
}



//! Suppression d'un film
exports.deleteFilm = (req, res, next) => {
    sess = req.session;
    var filmId = req.body.id;
    console.log(filmId);
    request.del({
        url: 'http://localhost:4000/movies/' + filmId
    }, function (req1, res1, body) {
        res.redirect('/films')
        console.log(body)
    })
}
//! Edition d'un film
exports.editFilm = function (req, res, next) {
    sess = req.session;
    let form = req.query;
    console.log(form)
    request.put({
        url: 'http://localhost:4000/movies/',
        form: form
    }, (error, response, body) => {
        res.redirect('/films')
    })
};