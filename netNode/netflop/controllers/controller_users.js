var request = require('request');

var sess;
var userName;

exports.index = (req, res, next) => {
    sess = req.session;
    //! logué
    if (sess.login) {
        return res.render('main', {
            name: userName
        });
    }
    //! pas logué
    res.render('index');

};

exports.login_form = function (req, res, next) {
    sess = req.session
    res.render('connect_form')
}

exports.login_user = function (req, res, next) {
    // if (sess !== undefined) {
    //     log = true
    // }
    sess = req.session

    let form = req.body;
    //! Connection a la base de donnée
    request.post({
        url: 'http://localhost:4000/login',
        form: form
    }, (error, response, body) => {
        if (body === "") {
            return res.render('connect_form', {
                badlog: true
            })
        }
        sess.login = req.body.login
        userName = req.body.login
        res.redirect('/')

    })
}


exports.logout = function (req, res, next) {

    req.session.destroy((err) => {
        if (err) {
            return console.log(err);
        }
        console.log('user deconnecté')
        res.redirect('/');
    });
};



exports.register_form = function (req, res, next) {
    sess = req.session
    res.render('register_form')
}

exports.register_user = function (req, res, next) {
    sess = req.session
    let form = req.body
    request.post({
        url: 'http://localhost:4000/users',
        form: form
    }, (error, response, body) => {
        if (error) {
            console.error(error)
            return
        }
        console.log(`statusCode: ${res.statusCode}`)
        console.log(body)
        res.render('index');
    })
}

exports.profil = function (req, res, next) {
    sess = req.session

    request.get("http://localhost:4000/users", function (req1, res1, body) {
        let users = JSON.parse(res1.body);
        for (let i = 0; i < users.length; i++) {
            if (users[i].login === userName) {
                let user = users[i]

                res.render('profil', {
                    id: user.id,
                    login: user.login,
                    email: user.email,
                    password: user.password,
                    name :user.name,
                    firstname: user.firstname,
                    zipcode: user.zipcode,

                })
            }
        }
    })
}

exports.edit = (req, res, next) => {
    sess = req.session
    let form = req.query
console.log(form)
    request.put({
        url: 'http://localhost:4000/users/',
        form: form
    }, (error, response, body) => {
        request.get("http://localhost:4000/users", function (req2, res2, body2) {
                res.render('profil', {
                        listUsers: JSON.parse(res2.body),
                        message: "Utilisateur édité"
                    })
                })
    })
}