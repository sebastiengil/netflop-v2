var express = require('express');
var router = express.Router();

var controller_films = require('../controllers/controller_films');

router.get('/',controller_films.index);

//router.get('/pageFilms', controller_films.page_films);


//GET request for list of film wanted
router.get('/films?:id', controller_films.list_films);

//GET request for one FILM
router.get('/film?:id', controller_films.film_details);



