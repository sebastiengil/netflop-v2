var express = require('express');
var users = express.Router();

// Require controller modules
var controller_users = require('../controllers/controller_users');

users.get('/', controller_users.index);

users.get('/logout', controller_users.logout);

users.get('/profil', controller_users.profil);

users.get('/edit', controller_users.edit)

users.get('/register', controller_users.register_form);

users.post('/register', controller_users.register_user)

users.get('/connexion', controller_users.login_form);

users.post('/connexion', controller_users.login_user);


module.exports = users;
