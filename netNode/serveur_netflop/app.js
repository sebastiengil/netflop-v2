var express = require('express');
var app = express();
var request = require('request');

// module pour recuperer des arguments dans le corps de la requete
var bodyParser = require('body-parser');

//Gestion des upload de fichiers
var multer = require('multer')

// multipart / form-data
var upload = multer();

// Couche d'accée aux données
const DAO = require('./DAO')
const dao = new DAO();

//Alimentera la propriete body de la requete
app.use(bodyParser.urlencoded({
    extended: true
})); //alimentera la propriete body de la requete

/**
 * element middleware :
 * cette fonction est appele des qu'une requete est recu
 * l'appel de la fonction next() permet de continuer vers la fonction cible de la requete
 * 
 * Premierement elle assure le CDRS pour les URL du siteweb Et de l'administration . Toutes les autres requetes seront bloquées.
 * pour le siteweb il faut accepeter les requetes GET sur les films et les users, puis  POST 
 * pour l'administration on peut tout accepter
 * 
 * Avec cette configuration on s'assure de la securité de l'api au niveau des accees.
 */
app.use((req, res, next) => {
    next();
    // res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,POST,PUT,DELETE');
    // let origin = req.headers.origin;
    // let ok = (origin === "http://netflop.bwb" || origin === "http://administration.netfop.bwb") ? true : false;
    // if (ok) { //les requetes viennent de notre application
    //     res.setHeader('Access-Control-Allow-Origin', origin)
    //     if (orrigin === "http://administration.netfop.bwb") { //l'admin peut faire ce quil veux
    //         next();
    //     } else { //sur le siteweb, c'est a dire sur netflop.bwb
    //         if (req.method === "GET") { //securite des GET
    //             if (req.path.includes("/user")) { //si on vient du domaine netflop.bwb/users
    //                 next();
    //             } else if (req.path.include("/film")) { //ou du domaine netflop.bwb/film
    //                 next();
    //             } else {
    //                 res.status(403).send("vous n'avez pas le droit d'acces a cette ressource");
    //             }
    //         } else if (req.method === "POST") { //securite POST
    //             if (req.path.includes("/user")) { //si on vient du domaine netflop.bwb/users
    //                 next();
    //             } else if (req.path.include("/film")) { //ou du domaine netflop.bwb/film
    //                 next();
    //             } else {
    //                 res.status(403).send("vous n'avez pas le droit d'acces a cette ressource");
    //             }
    //         } else { //securité des autres types de requetes
    //             res.status(403).send("vous n'avez pas le droit d'acces a cette ressource");
    //         }
    //     }
    // } else { //securité des autres types de requetes
    //     res.status(403).send("vous n'avez pas le droit d'acces a cette ressource");
    // }
});

/**
 * Fin de la partie concernant le middlware
 */

/**
 * Retourne la racine de l'application(permet de s'assurer que le serveur tourne)
 */
app.get("/", (req, res) => {
    res.setHeader("Content-Type", "text/plain");
    res.send("Bienvenue sur l'API")
});
/**
 * premet de reinitialiser la base de donnée avec les valeurs par defaut
 */
app.put("/initdb", (req, res) => {
    dao.initDB();
    res.setHeader('Content-Type', 'text/plain');
    res.send('la base de données a bien été reinitialisée')
});


/**
 * Retourne la liste des utilisateurs stockés en base de données
 */
app.get("/users", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getUsers((datas) => {
        res.send(datas);
    });
});
/**
 * Retourne l'utilisateur stocké en base de données dont l'ID correspond a celui dans l'url
 */
app.get("/users/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getUsersById(req.params.id, (data) => {
        if (data !== null) {
            res.send(data);
        } else {
            res.status(404).send({
                message: "Le User n'existe pas"
            })
        }

    });
});

/**
 * ajouter un nouvelle utilisateur en base de donné avec les informations contenus dnas le corps de la requete POST
 */
app.post("/users", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.createUser(req.body, (data) => {
        res.send(data);
    });
});

/**
 * Met a jour l'utilisateur passé dans le corps de la requete
 */
app.put("/users", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.updateUser(req.body, (data) => {
        res.send(data);
    });
});

/**
 * Delete un utilisateur dont l'id est passé dans l'URL
 */
app.delete("/users/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.deleteUser(req.params.id, (datas) => {
        res.send(datas);
    });
});

/**
 * USERS
 */

app.post("/login", (req, res) => {
    res.setHeader('Content-Type', 'text/plain');
    dao.loginUser(req.body, (datas) => {
        res.send(datas);
    })
});


/**
 * FILM !!!
 * Table movie_id
 * id / title / year / poster / description / genre_id / director
 * 
 */

//retourne la liste des films
app.get("/movies", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getMovies((datas) => {
        res.send(datas);
    });
});
/**
 * Retourne le film stocké en base de données dont l'ID correspond a celui dans l'url
 */
app.get("/movies/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getMovieById(req.params.id, (data) => {
        if (data !== null) {
            res.send(data);
        } else {
            res.status(404).send({
                message: "Le film n'existe pas"
            })
        }

    });
});

/**
 * ajouter un nouveau film en base de donné avec les informations contenus dnas le corps de la requete POST
 */
app.post("/movies", (req, res) => {
    res.setHeader('Content-Type', 'plain/text');
    dao.createMovie(req.body, (data) => {
        res.send(data);
    });
});
/**
 * Met a jour l'utilisateur passé dans le corps de la requete
 */
app.put("/movies", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.updateMovie(req.body, (data) => {
        res.send(data);
    });
});

/**
 * Delete un utilisateur dont l'id est passé dans l'URL
 */
app.delete("/movies/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.deleteMovie(req.params.id, (datas) => {
        res.send(datas);
    });
});


/**
 * NOTE !!!
 * id/movie_id/user_id/note
 */
app.get("/note", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getMovies((datas) => {
        res.send(datas);
    });
});

app.get("/note/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.getNoteById(req.params.id, (data) => {
        if (data !== null) {
            res.send(data);
        } else {
            res.status(404).send({
                message: "Le like n'existe pas"
            })
        }

    });
});


/**
 * ajouter un nouveau like dans le corps de la requete POST
 */
app.post("/note", (req, res) => {
    res.setHeader('Content-Type', 'plain/text');
    dao.createNote(req.body, (data) => {
        res.send(data);
    });
});

app.delete("/note/:id", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    dao.deleteNote(req.params.id, (datas) => {
        res.send(datas);
    });
});






module.exports = app;


console.log("Serveur NE route sur http://localhost:4000");


app.listen(4000);