module.exports = class DAO {
    /**
     * @callback AccessLayerGetDatas
     * @param {Object{}} rows une liste de donnée issue de la base de donnée 
     */
    /**
     * @callback AccessLayerGetData
     * @param {Object {}} rows une donnée issue de la base de donnée
     */
    /**
     * @callback AccessLayerCUD
     * @param {any} error message d'erreur ou undefined si tout c'est bien passé
     */

    constructor() {
        var file = require('fs');
        this.sqlite3 = require('sqlite3').verbose();
        this.dbFile = "./db/netflop.s3db"
    }
    openDB() {
        this.sqlite3 = require('sqlite3').verbose();
        this.db = new this.sqlite3.Database(this.dbFile);
    }
    initDB(callback) {
        this.openDB();
        this.db.serialize(() => {
            this.db.close();
        });
        if (callback !== undefined) {
            callback();
        }
    }


    // CRUD USER - CRUD USER - CRUD USER - CRUD USER
    /**
     * 
     * @param {AccessLayerGetDatas} callback
     */

    getUsers(callback) {
        this.openDB();

        this.db.serialize(() => {
            // var result = [];
            this.db.all("SELECT * FROM user", function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            this.db.close();
        })
    }

    /**
     * @param {Number} id l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */

    getUsersById(id, callback) {
        this.openDB();

        this.db.serialize(() => {
            //var result = [];
            this.db.all("SELECT * FROM user WHERE id=" + id, function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            this.db.close();
        })
    }

    /**
     * @param {any} e données au format json à inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    createUser(e, callback) {
        this.openDB();
        this.db.serialize(() => {
            this.db.run("INSERT INTO user(login,password,name,firstname,email,zipcode) VALUES('" + e.login + "','" + e.password + "','" + e.name + "','" + e.firstname + "','" + e.email + "','" + e.zipcode + "')", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK CREATE");
                }
            });
        });
        this.db.close();
    }

    /**
     * @param {any} e données au format json a inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    updateUser(e, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.run("UPDATE user SET login='" + e.login + "',password='" + e.password + "',name='" + e.name + "',firstname='" + e.firstname + "',email='" + e.email + "',zipcode='" + e.zipcode + "' WHERE id=" + e.id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK UPDATE");
                }
            });
        });
        this.db.close();
    }

    /** 
     * @param {any} e Donnée dans l'URL
     * @param {AccessLayerGetDatas} callback 
     */
    loginUser(e, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.get("SELECT * FROM user WHERE login = '" + e.login + "' AND password = '" + e.password + "'", (err, rows) => {
                if (err !== null) {
                    callback(err);
                }
                callback(rows);

            });
        });
        this.db.close();
    }


    /** 
     * @param {number} id //id de l'utilisateur à supprimer
     * @param {AccessLayerCUD} callback 
     */
    deleteUser(id, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.run("DELETE FROM user WHERE id=" + id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK DELETE");
                }
            });
        });
        this.db.close();
    }

    /**
     *  MOVIES !
     */

    /**
     * 
     * @param {AccessLayerGetDatas} callback
     */

    getMovies(callback) {
        this.openDB();

        this.db.serialize(() => {
            // var result = [];
            this.db.all("SELECT * FROM movie", function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            this.db.close();
        })
    }
    /**
     * @param {Number} id l'id de l'utilisateur selectionne
     * @param {AccessLayerGetData} callback 
     */

    getMovieById(id, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.get("SELECT * FROM movie WHERE id=" + id, function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            this.db.close();
        })
    }
    /**
     * @param {any} e données au format json à inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    createMovie(e, callback) {
        this.openDB();
        this.db.serialize(() => {
            this.db.run("INSERT INTO movie(title,year,poster,description,genre_id,director) VALUES('" + e.title + "','" + e.year + "','" + e.poster + "','" + e.description + "','" + e.genre_id + "','" + e.director + "')", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK CREATE");
                }
            });
        });
        this.db.close();
    }
    /**
     * @param {any} e données au format json a inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    updateMovie(e, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.run("UPDATE movie SET title='" + e.title + "',year='" + e.year + "',poster='" + e.poster + "',description='" + e.description + "',genre_id='" + e.genre_id + "',director='" + e.director + "' WHERE id=" + e.id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK UPDATE");
                }
            });
        });
        this.db.close();
    }
    /** 
     * @param {number} id //id de l'utilisateur à supprimer
     * @param {AccessLayerCUD} callback 
     */
    deleteMovie(id, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.run("DELETE FROM movie WHERE id=" + id, (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK DELETE");
                }
            });
        });
        this.db.close();
    }

    /**
     *  NOTE
     * id/movie_id/user_id/note
     */
 
    /**
     * 
     * @param {AccessLayerGetDatas} callback
     */

    getNote(callback) {
        this.openDB();

        this.db.serialize(() => {
            // var result = [];
            this.db.all("SELECT * FROM like", function (err, rows) {
                if (err !== null) {
                    callback(err);
                } else {
                    callback(rows);
                }
            });
            this.db.close();
        })
    }
  
   /**
     * @param {any} e données au format json à inserer en base de donnée
     * @param {AccessLayerCUD} callback 
     */

    createNote(e, callback) {
        this.openDB();
        this.db.serialize(() => {
            this.db.run("INSERT INTO like", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK CREATE");
                }
            });
        });
        this.db.close();
    }

    /** 
     * @param {number} id //id de l'utilisateur à supprimer
     * @param {AccessLayerCUD} callback 
     */
    deleteNote(id, callback) {
        this.openDB();

        this.db.serialize(() => {
            this.db.run("DELETE FROM like WHERE", (err) => {
                if (err !== null) {
                    callback(err);
                } else {
                    callback("OK DELETE");
                }
            });
        });
        this.db.close();
    }

}